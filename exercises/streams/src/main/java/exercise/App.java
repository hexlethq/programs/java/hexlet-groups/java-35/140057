package exercise;

import java.util.List;

// BEGIN
class App {

    private static List<String> freeEmailHostings = List.of("gmail.com", "yandex.ru", "hotmail.com");

    public static int getCountOfFreeEmails(List<String> emails) {
        if (emails == null) {
            return 0;
        }
        return (int) emails.stream()
                     .map(email -> getHosting(email))
                     .filter(email -> freeEmailHostings.contains(email))
                     .count();
    }
    private static String getHosting(String email) {
        String[] emailAddressParts = email.split("@");
        return emailAddressParts[1];
    }
}
// END
