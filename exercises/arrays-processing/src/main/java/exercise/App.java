package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int getIndexOfMaxNegative(int[] arr) {
        int result = -1;
        int currentNegativeMax = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < 0 && arr[i] > currentNegativeMax) {
                currentNegativeMax = arr[i];
                result = i;
            }
        }
        return result;
    }
    private static double getArrayAverage(int[] arr) {
        int sum = 0;
        for (int arrElem : arr) {
            sum += arrElem;
        }
        return  arr.length != 0 ? sum / arr.length : 0;
    }
    public static int[] getElementsLessAverage(int[] arr) {
        int[] result = new int[0];
        if (arr.length < 1) {
            return result;
        }
        result = new int[arr.length];
        double arrAverage = getArrayAverage(arr);
        int counter = 0;
        for (int arrElem: arr) {
            if (arrElem <= arrAverage) {
                result[counter] = arrElem;
                counter += 1;
            }
        }
        result = Arrays.copyOfRange(result, 0, counter);
        return result;
    }
    private static int[][] getArrayMinMax(int[] arr) {
        // result[0][0] - minimum value, result[0][1] - minimum index
        // result[1][0] - maximum value, result[1][1] - maximum index
        int[][] result = {{Integer.MAX_VALUE, -1}, {Integer.MIN_VALUE, -1}};
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] <= result[0][0]) {
                result[0][0] = arr[i];
                result[0][1] = i;
            }
            if (arr[i] >= result[1][0]) {
                result[1][0] = arr[i];
                result[1][1] = i;
            }
        }
        return  result;
    }
    private static int getArraySum(int[] arr) {
        int sum = 0;
        for (int arrElem : arr) {
            sum += arrElem;
        }
        return  sum;
    }

    public static int getSumBeforeMinAndMax(int[] arr) {
        int result = 0;
        if (arr.length < 1) {
            return result;
        }
        int start = 0;
        int finish = 0;
        int[][] arrMinMax = getArrayMinMax(arr);
        if (arrMinMax[0][1] > -1 && arrMinMax[1][1] > -1) {
            if (arrMinMax[0][1] > arrMinMax[1][1]) {
                finish = arrMinMax[0][1];
                start  = arrMinMax[1][1];
            } else {
                finish = arrMinMax[1][1];
                start  = arrMinMax[0][1];
            }
            int[] tempArr = Arrays.copyOfRange(arr, start + 1, finish);
            result = getArraySum(tempArr);
        }
        return result;
    }
    // END
}
