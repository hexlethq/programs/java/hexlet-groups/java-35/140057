package exercise;

class Sentence {

    public static void printSentence(String sentence) {
        // BEGIN
        if (sentence.substring(sentence.length() - 1).equals("!")) {
            System.out.println(sentence.toUpperCase());
        } else {
            System.out.println(sentence.toLowerCase());
        }
        // END
    }
}
