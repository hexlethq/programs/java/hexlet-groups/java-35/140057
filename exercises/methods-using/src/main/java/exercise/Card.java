package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        String hiddenTempl = "*";
        String asterixPart = hiddenTempl.repeat(starsCount);
        String lastCardDigits = cardNumber.substring(12, cardNumber.length());
        System.out.println(asterixPart + lastCardDigits);
        // END
    }
}
