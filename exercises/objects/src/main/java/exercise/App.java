package exercise;

import java.util.Locale;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

class App {
    // BEGIN
    public static String buildList(String[] arr) {
        String result = "";
        StringBuilder stringBuilder = new StringBuilder();
        for (String arrElem: arr) {
            stringBuilder.append("  <li>").append(arrElem).append("</li>\n");
        }
        if (stringBuilder.length() > 0) {
            stringBuilder.insert(0, "<ul>\n");
            stringBuilder.append("</ul>");
            result = stringBuilder.toString();
        }
        return result;
    }
    public static String getUsersByYear(String[][] users, int year) {
        String result = "";
        String[] userNames = new String[users.length];
        int counter = 0;
        for (String[] user: users) {
            LocalDate locDate = LocalDate.parse(user[1]);
            if (locDate.getYear() == year) {
                userNames[counter] = user[0];
                counter += 1;
            }
        }
        result = buildList(Arrays.copyOfRange(userNames, 0, counter));
        return result;
    }
    // END

    // Это дополнительная задача, которая выполняется по желанию.
    public static String getYoungestUser(String[][] users, String date)
    throws Exception {
        // BEGIN
        String result = "";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy", Locale.ENGLISH);
        LocalDate inputDate          = LocalDate.parse(date, formatter);
        LocalDate maxBeforeInput = LocalDate.parse("1800-01-01");
        for (String[] user: users) {
            LocalDate locDate = LocalDate.parse(user[1]);
            if (locDate.isBefore(inputDate) && locDate.isAfter(maxBeforeInput)) {
                result = user[0];
                maxBeforeInput = locDate;
            }
        }
        return result;
        // END
    }
}
