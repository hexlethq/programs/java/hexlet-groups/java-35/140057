package exercise;

class Converter {
    // BEGIN
    public static int convert(int valueToConvert, String unitOfMeasure) {
        int conversionResult;
        switch (unitOfMeasure) {
            case "Kb":
                conversionResult = valueToConvert / 1024;
                break;
            case "b":
                conversionResult = valueToConvert * 1024;
            break;
            default:
                conversionResult = 0;
        }
        return conversionResult;
    }
    public static void main(String[] args) {
        String toPrint = "%d Kb = %d b";
        toPrint = String.format(toPrint, 10, Converter.convert(10, "b"));
        System.out.println(toPrint);
    }
    // END
}
