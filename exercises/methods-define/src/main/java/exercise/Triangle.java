package exercise;

class Triangle {
    // BEGIN
    public static double getSquare(int aSide, int bSide, int angle) {
        double square = 0;
        square = 0.5 * aSide * bSide * Math.sin(angle * Math.PI / 180);
        return square;
    }
    public static void main(String[] args) {
        System.out.println(Triangle.getSquare(4, 5, 45));
    }
    // END
}
