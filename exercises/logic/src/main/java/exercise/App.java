package exercise;

class App {
    public static boolean isBigOdd(int number) {
        // BEGIN
        Boolean isBigOddVariable = false;
        if (number % 2 == 1 && number >= 1001) {
            isBigOddVariable = true;
        }
        // END
        return isBigOddVariable;
    }

    public static void sayEvenOrNot(int number) {
        // BEGIN
        System.out.println(number % 2 == 0 ? "yes" : "no");
        // END
    }

    public static void printPartOfHour(int minutes) {
        // BEGIN
        if (minutes >= 0 && minutes < 15) {
            System.out.println("First");
        } else if (minutes >= 15 && minutes < 30) {
            System.out.println("Second");
        } else if (minutes >= 30 && minutes < 45) {
            System.out.println("Third");
        } else if (minutes >= 45 && minutes < 60) {
            System.out.println("Fourth");
        }
        // END
    }
}
