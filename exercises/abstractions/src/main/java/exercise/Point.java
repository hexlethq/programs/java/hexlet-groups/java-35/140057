package exercise;

class Point {
    // BEGIN
    public static int[] makePoint(int x, int y) {
        return new int[] {x, y};
    }
    public static int getX(int[] point) {
        return point[0];
    }
    public static int getY(int[] point) {
        return point[1];
    }
    public static String pointToString(int[] point) {
        return (String.format("(%d, %d)", point[0], point[1]));
    }
    public static int getQuadrant(int[] point) {
        int result = 0;
        if (point[0] > 0 && point[1] > 0) {
            result = 1;
        } else if (point[0] < 0 && point[1] > 0) {
            result = 2;
        } else if (point[0] < 0 && point[1] < 0) {
            result = 3;
        } else if (point[0] > 0 && point[1] < 0) {
            result = 4;
        } else {
            result = 0;
        }
        return result;
    }
    public static int[] getSymmetricalPointByX(int[] point) {
        return new int[] {-getX(point), getY(point)};
    }
    public static double calculateDistance(int[] pointA, int[] pointB) {
        double diffX = Math.pow(getX(pointA) - getX(pointB), 2);
        double diffY = Math.pow(getY(pointA) - getY(pointB), 2);
        return Math.sqrt(diffX + diffY);
    }
    // END
}
