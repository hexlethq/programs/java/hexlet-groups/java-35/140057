package exercise;

import java.util.Arrays;


// BEGIN
class Kennel {

    private static String[][] puppies = new String[0][0];

    public static void addPuppy(String[] puppy) {
        int count = puppies.length;
        puppies = Arrays.copyOf(puppies, puppies.length + 1);
        puppies[count] = puppy;
    }
    public static void addSomePuppies(String[][] puppiesIn) {
        int count = puppies.length;
        puppies = Arrays.copyOf(puppies, count + puppiesIn.length);
        for (int i = 0; i < puppiesIn.length; i++) {
            puppies[count + i] = puppiesIn[i];
        }
    }
    public static int getPuppyCount() {
        return puppies.length;
    }
    public static boolean isContainPuppy(String name) {
        for (String[] puppy: puppies) {
            if (puppy[0].equals(name)) {
                return true;
            }
        }
        return false;
    }
    public static String[][] getAllPuppies() {
        return puppies;
    }
    public static String[] getNamesByBreed(String breed) {
        String[] result = new String[puppies.length];
        int counter = 0;
        for (String[] puppy: puppies) {
            if (puppy[1].equals(breed)) {
                result[counter] = puppy[0];
                counter += 1;
            }
        }
        result = Arrays.copyOf(result, counter);
        return result;
    }
    public static void resetKennel() {
        puppies = new String[0][0];
    }
    public static boolean removePuppy(String name) {
        String[][] updatedPuppies = new String[puppies.length][2];
        int counter = 0;
        if (isContainPuppy(name)) {
            for (String[] puppy: puppies) {
                if (!puppy[0].equals(name)) {
                    updatedPuppies[counter] = puppy;
                    counter += 1;
                }
            }
            puppies = Arrays.copyOf(updatedPuppies, counter);
            return true;
        }
        return false;
    }
    public static void main(String[] args) {
    }
}
// END
