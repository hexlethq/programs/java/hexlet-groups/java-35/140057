package exercise;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.Map.Entry;

// BEGIN
class App {
    public static List<Map<String, String>> findWhere(List<Map<String, String>> books, Map<String, String> where) {
        List<Map<String, String>> result = new ArrayList<>();
        for (Map<String, String> book: books) {
            if (isBookValid(book, where)) {
                result.add(book);
            }
        }
        return result;
    }
    private static boolean isBookValid(Map<String, String> book, Map<String, String> where) {
        for (Map.Entry<String, String> bookAttributePair: where.entrySet()) {
            String bookAttributeKey = bookAttributePair.getKey();
            if (!book.containsKey(bookAttributeKey)) {
                return false;
            }
            if (!book.get(bookAttributeKey).equals(bookAttributePair.getValue())) {
                return false;
            }
        }
        return true;
    }
}
//END
