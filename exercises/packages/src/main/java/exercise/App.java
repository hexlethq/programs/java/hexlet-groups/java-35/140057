// BEGIN
package exercise;

import exercise.geometry.Point;
import exercise.geometry.Segment;

public class App {

    public static double[] getMidpointOfSegment(double[][] segment) {
        double[] begin = Segment.getBeginPoint(segment);
        double[] end = Segment.getEndPoint(segment);

        double xMid = (Point.getX(begin) + Point.getX(end)) / 2;
        double yMid = (Point.getY(begin) + Point.getY(end)) / 2;
        return Point.makePoint(xMid, yMid);
    }
    public static double[][] reverse(double[][] segment) {
        double[] oldBegin = Segment.getBeginPoint(segment);
        double[] oldEnd = Segment.getEndPoint(segment);

        double oldBeginX = Point.getX(oldBegin);
        double oldBeginY = Point.getY(oldBegin);
        double oldEndX = Point.getX(oldEnd);
        double oldEndY = Point.getY(oldEnd);

        double[] newBegin = Point.makePoint(oldEndX, oldEndY);
        double[] newEnd = Point.makePoint(oldBeginX, oldBeginY);

        return Segment.makeSegment(newBegin, newEnd);
    }
}
// END
