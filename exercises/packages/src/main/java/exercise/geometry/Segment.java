// BEGIN
package exercise.geometry;

public class Segment {
    public static double[][] makeSegment(double[] p1, double[] p2) {
        return new double[][] {p1, p2};
    }
    public static double[] getBeginPoint(double[][] segment) {
        return segment[0];
    }
    public static double[] getEndPoint(double[][] segment) {
        return segment[1];
    }

}
// END
