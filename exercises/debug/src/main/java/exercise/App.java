package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int a, int b, int c) {
        String result = "";
        if (!isValidTriangle(a, b, c)) {
            result = "Треугольник не существует";
        } else if (a == b && b == c) {
            result = "Равносторонний";
        } else if (a == b || b == c || a == c) {
            result = "Равнобедренный";
        } else {
            result = "Разносторонний";
        }
        return result;
    }
    private static Boolean isValidTriangle(int a, int b, int c) {
        Boolean validTriangle = false;
        if (a + b > c && a + c > b && b + c > a) {
            validTriangle = true;
        }
        return validTriangle;
    }
    public static int getFinalGrade(int exam, int project) {
        int finalMark = 0;
        if (exam >= 90 || project > 10) {
            finalMark = 100;
        } else if (exam > 75 && project >= 5) {
            finalMark = 90;
        } else if (exam > 50 && project >= 2) {
            finalMark = 75;
        } else {
            finalMark = 0;
        }
        return finalMark;
    }
    // END
}
