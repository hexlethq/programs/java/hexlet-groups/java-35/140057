package exercise;

import java.util.ArrayList;
import java.util.List;
// BEGIN
class App {
    public static boolean scrabble(String letters, String word) {

        List<Character> lettersList = new ArrayList<>();

        for (char ch : letters.toCharArray()) {
            lettersList.add(ch);
        }

        for (char ch: word.toLowerCase().toCharArray()) {
            if (!lettersList.remove(Character.valueOf(ch))) {
                return false;
            }
        }
        return true;
    }
}
//END
