package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String str) {
        String result = "";
        boolean isNextValid = true;
        char currentSymbol;
         for (int i = 0; i < str.length(); i++) {
            currentSymbol = str.charAt(i);
            if (currentSymbol == ' ') {
                isNextValid = true;
            } else if (isNextValid) {
                result = result + Character.toUpperCase(currentSymbol);
                isNextValid = false;
            }
        }
        return result;
    }
    // END
}
