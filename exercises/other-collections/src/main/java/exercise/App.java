package exercise;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

// BEGIN
public class App {
    public static Map<String, String> genDiff(Map<String, Object> mapA, Map<String, Object> mapB) {
        Map<String, String> result = new LinkedHashMap<>();
        Set<String> keySet = new TreeSet<>();
        keySet.addAll(mapA.keySet());
        keySet.addAll(mapB.keySet());

        for (String key : keySet) {
            result.put(key, getDiffState(mapA.get(key), mapB.get(key)));
        }
        return  result;
    }
    private static String getDiffState(Object valueA, Object valueB) {
        if (valueA == null) {
            return "added";
        }
        if (valueB == null) {
            return "deleted";
        }
        if (valueA.equals(valueB)) {
            return "unchanged";
        } else {
            return "changed";
        }
    }
}
//END
