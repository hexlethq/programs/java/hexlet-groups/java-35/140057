package exercise;

import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
class App {
    public static String[][] enlargeArrayImage(String[][] image) {
        return Arrays.stream(image)
                .map(line -> duplicate1DArray(line))
                .flatMap(element -> Stream.of(element, element))
                .toArray(String[][]::new);
    }
    private static String[] duplicate1DArray(String[] line) {
        String[] enlargedImage = Arrays.stream(line)
                .flatMap(element -> Stream.of(element, element))
                .toArray(String[]::new);
            return enlargedImage;
    }
}
// END
