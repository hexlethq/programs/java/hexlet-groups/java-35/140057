package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int[] sort(int[] arr) {
        int[] result = Arrays.copyOf(arr, arr.length);
        boolean isExchHappened = false;
        do {
            isExchHappened = false;
            for (int i = 0; i < arr.length - 1; i++) {
                if (result[i] > result[i + 1]) {
                    int tmp = result[i];
                    result[i] = result[i + 1];
                    result[i + 1] = tmp;
                    isExchHappened = true;
                }
            }
        } while (isExchHappened);
        return result;
    }
    public static int[] selectionSort(int[] arr) {
        int[] result = Arrays.copyOf(arr, arr.length);
        for (int j = result.length - 1; j >= 0; j--) {
            int localMaximum = Integer.MIN_VALUE;
            int localMaximumIndex = 0;
            for (int i = 0; i <= j; i++) {
                if (result[i] > localMaximum) {
                    localMaximum = result[i];
                    localMaximumIndex = i;
                }
            }
            result[localMaximumIndex] = result[j];
            result[j] = localMaximum;
        }
        return result;
    }
    public static void main(String[] args) {

        int[] numbers1 = {};
        int[] result1 = App.selectionSort(numbers1);
        System.out.println(Arrays.toString(result1));

        int[] numbers2 = {10, 1, 3};
        int[] result2 = App.selectionSort(numbers2);
        System.out.println(Arrays.toString(result2));

        int[] numbers3 = {0, 4, 0, 10, -3};
        int[] result3 = App.selectionSort(numbers3);
        System.out.println(Arrays.toString(result3));

        int[] numbers4 = {99, 4, 0, 10, -3};
        int[] result4 = App.selectionSort(numbers4);
        System.out.println(Arrays.toString(result4));

        int[] numbers5 = {99, 4, 0, 10, -3, 99};
        int[] result5 = App.selectionSort(numbers5);
        System.out.println(Arrays.toString(result5));

        int[] numbers6 = {-3, -3, -3, -2};
        int[] result6 = App.selectionSort(numbers6);
        System.out.println(Arrays.toString(result6));

    }
    // END
}
