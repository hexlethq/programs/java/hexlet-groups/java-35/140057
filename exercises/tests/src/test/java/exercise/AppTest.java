package exercise;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;

class AppTest {

    @Test
    void testTake() {
        // BEGIN
        testSimple();
        testWithEmptyList();
        testWithIndexBeyondUpperBoundary();
        // END
    }
    private void testSimple() {
        List<Integer> actual = App.take(new ArrayList<>(Arrays.asList(1, 2, 3, 4)), 2);
        List<Integer>  expected = new ArrayList<>(Arrays.asList(1, 2));
        assertThat(actual).isEqualTo(expected);
    }
    private void testWithEmptyList() {
        List<Integer> actual = App.take(new ArrayList<>(), 2);
        List<Integer>  expected = new ArrayList<>();
        assertThat(actual).isEqualTo(expected);
    }
    private void testWithIndexBeyondUpperBoundary() {
        List<Integer> actual = App.take(new ArrayList<>(Arrays.asList(1, 2, 3, 4)), 10);
        List<Integer>  expected = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        assertThat(actual).isEqualTo(expected);
    }
}
