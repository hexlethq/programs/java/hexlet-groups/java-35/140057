package exercise;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

// BEGIN
class App {
    public static Map<String, Integer> getWordCount(String sentence) {

        Map<String, Integer> result = new HashMap<>();

        if (sentence.length() == 0) {
            return result;
        }

        String[] splittedSentence = sentence.split(" ");

        for (String word: splittedSentence) {
            Integer wordCounter = result.get(word);
            int intValue = wordCounter == null ? 0 : wordCounter.intValue();
            result.put(word, ++intValue);
        }
        return result;
    }
    public static String toString(Map<String, Integer> wordMap) {
        String result = "{";

        Set<String> keySet = wordMap.keySet();

        for (String key: keySet) {
            result += String.format("\n  %s: %d", key, wordMap.get(key));
        }

        result += result.equals("{") ? "}" : "\n}";

        return result;
    }
}
//END
