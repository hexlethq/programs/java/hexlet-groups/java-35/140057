package exercise;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.apache.commons.io.IOUtils;

class AppTest {
    private static String execCommand() throws Exception {
        String command = "build/install/building/bin/building";
        Process process = Runtime.getRuntime().exec(command);
        String output = IOUtils.toString(process.getInputStream());
        process.waitFor();
        return output;
    }

    @Test
    void testOutput() throws Exception {
        String output = execCommand().trim();
        assertThat(output).isEqualTo("Hello, World!");
    }

    // BEGIN
    @Test
    void testToJson() throws Exception {
        String[] fruits = {"apple", "pear", "lemon"};
        String actual = App.toJson(fruits);
        String expected = "[\"apple\",\"pear\",\"lemon\"]";
        assertThat(actual).isEqualTo(expected);

        String[] empty = {};
        String actual1 = App.toJson(empty);
        String expected1 = "[]";
        assertThat(actual1).isEqualTo(expected1);

    }
    // END
}
