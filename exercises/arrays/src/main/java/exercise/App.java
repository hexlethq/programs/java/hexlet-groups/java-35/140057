package exercise;

class App {
    // BEGIN
    public static int[] reverse(int[] arr) {
        int[] result = new int[arr.length];
        for (int i = arr.length - 1; i >= 0; i--) {
            result[arr.length - 1 - i] = arr[i];
        }
        return result;
    }
    public static int mult(int[] arr) {
        int result = 1;
        for (int arrElem: arr) {
            result *= arrElem;
        }
        return result;
    }
    private static Boolean is2dArrayValid(int[][] arr2d) {
        return arr2d.length < 1 || arr2d[0].length < 1;
    }
    public static int[] flattenMatrix(int[][] arr2d) {
        if (is2dArrayValid(arr2d)) {
            return new int[0];
        }
        int[] result = new int[arr2d.length * arr2d[0].length];
        int counter = 0;
        for (int[] arr1d: arr2d) {
            for (int arrElem: arr1d) {
                result[counter] = arrElem;
                counter++;
            }
        }
        return result;
    }
    // END
}
