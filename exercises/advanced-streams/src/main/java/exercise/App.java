package exercise;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// BEGIN
public class App {
    public static final String PREFIX = "X_FORWARDED_";
    public static final String ENVIRONMENT = "environment";

    public static String getForwardedVariables(String configFileContent) {
        return Stream.of(configFileContent)
              .map(s -> s.split("\n"))
              .flatMap(Arrays::stream)
              .filter(s -> s.startsWith(ENVIRONMENT) && s.contains(PREFIX))
              .map(s ->  s.substring(s.indexOf('"') + 1, s.lastIndexOf('"')))
              .map(s -> parseEnviromentAttributes(s))
              .collect(Collectors.joining(","));
    }
    private static String parseEnviromentAttributes(String line) {
        return Stream.of(line)
                .map(s -> s.split(","))
                .flatMap(Arrays::stream)
                .filter(s -> s.startsWith(PREFIX))
                .map(s -> s.substring(PREFIX.length()))
                .collect(Collectors.joining(","));
    }
}
//END
